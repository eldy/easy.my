import React, { Component } from 'react'
import Navbar from './components/Navbar'
import ProductInfo from './components/ProductInfo'
import SellerInfo from './components/SellerInfo'
import Social from './components/Social'
import ContactChannel from './components/ContactChannel'

import styles from './App.scss'

class App extends Component {
  /* let's assume this data came from the api... 
  usually it won't be in the state, but it comes in prop,
  but for the sake of this demo just put into state */

  state = {
    response: {
      product: {
        name:
          'Nintendo Switch Neon Joy-Con (1 Year MaxSoft Warranty) + Screen Protector',
        image: 'https://i.imgur.com/nFDDdI1.png',
        price: '1,289',
        condition: 'Brand new in the box',
        location: 'Bangsar south, Kuala lumpur'
      },
      seller: {
        name: 'Takeshi Nakamura',
        type: 'private seller',
        phone: '0178842340'
      }
    }
  }
  render() {
    const {
      response: { product, seller }
    } = this.state

    /* A page should be in a container but
     for the sake of demo purposes I'll just put everything here */

    return (
      <div className={styles.outerContainer}>
        <Navbar />

        <div className={styles.container}>
          <div className={styles.breadcrum}>
            Home > Electronics > Games & Console > ${product.name}
          </div>

          <h1 className={styles.heading}>{product.name}</h1>

          <div className={styles.wrap}>
            <div className={styles.productCarousel}>
              <img src={product.image} alt={product.name} />
            </div>

            <div className={styles.productSidebar}>
              <Social />
              <ProductInfo
                price={product.price}
                condition={product.condition}
                location={product.location}
              />
              <SellerInfo name={seller.name} type={seller.type} />
              <ContactChannel phone={seller.phone} />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default App
