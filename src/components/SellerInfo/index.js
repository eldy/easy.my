import React from 'react'
import styles from './SellerInfo.scss'
import avatar from '../../assets/avatar.svg'

const SellerInfo = ({ name, type }) => (
  <div className={styles.wrap}>
    <div className={styles.label}>Seller info</div>
    <div className={styles.userBlock}>
      <img src={avatar} alt={name} />
      <div className={styles.right}>
        <div className={styles.value}>{name}</div>
        <div className={`${styles.label} ${styles.sellerType}`}>{type}</div>
      </div>
    </div>
  </div>
)
export default SellerInfo
