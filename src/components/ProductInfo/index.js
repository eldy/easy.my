import React from 'react'
import styles from './ProductInfo.scss'

const ProductInfo = ({ price, condition, location }) => (
  <div className={styles.wrap}>
    <div className={styles.label}>Price</div>
    <div className={`${styles.value} ${styles.price}`}>
      RM
      {price}
    </div>
    <div className={styles.label}>Item Condition</div>
    <div className={styles.value}>{condition}</div>
    <div className={styles.label}>Item Location</div>
    <div className={styles.value}>{location}</div>
  </div>
)
export default ProductInfo
