import React from 'react'
import logo from '../../assets/logo.svg'
import styles from './Navbar.scss'

const Navbar = () => (
  <div className={styles.container}>
    <img src={logo} alt="logo" />
  </div>
)
export default Navbar
