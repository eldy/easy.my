import React from 'react'

const SVG = ({
  fill = '#000',
  width = '100%',
  height = '100%',
  viewBox = '0 0 24 24',
  classes
}) => (
  <svg
    width={width}
    height={height}
    className={classes}
    viewBox={viewBox}
    xmlns="http://www.w3.org/2000/svg"
    xmlnsXlink="http://www.w3.org/1999/xlink"
  >
    <path
      fill={fill}
      d="M18,16.1a3.176,3.176,0,0,0-2,.8L8.9,12.7A1.7,1.7,0,0,0,9,12a1.7,1.7,0,0,0-.1-.7L16,7.2a2.973,2.973,0,1,0,4-4.4,2.992,2.992,0,0,0-4.2.2A2.792,2.792,0,0,0,15,5a1.7,1.7,0,0,0,.1.7L8,9.8a2.971,2.971,0,1,0,0,4.3l7.1,4.2a1.268,1.268,0,0,0-.1.6,2.9,2.9,0,1,0,5.8,0A2.734,2.734,0,0,0,18,16.1Z"
    />
  </svg>
)

export default SVG
