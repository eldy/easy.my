import React from 'react'

const SVG = ({
  fill = '#000',
  width = '100%',
  height = '100%',
  viewBox = '0 0 24.458 24',
  classes
}) => (
  <svg
    width={width}
    height={height}
    className={classes}
    viewBox={viewBox}
    xmlns="http://www.w3.org/2000/svg"
    xmlnsXlink="http://www.w3.org/1999/xlink"
  >
    <path
      fill={fill}
      d="M6.689,10.79A15.3,15.3,0,0,0,13.4,17.38l2.242-2.2a1.027,1.027,0,0,1,1.039-.24,11.827,11.827,0,0,0,3.638.57,1.013,1.013,0,0,1,1.019,1V20a1.013,1.013,0,0,1-1.019,1A17.163,17.163,0,0,1,3,4,1.013,1.013,0,0,1,4.019,3H7.586A1.013,1.013,0,0,1,8.6,4a11.166,11.166,0,0,0,.581,3.57.99.99,0,0,1-.255,1.02Z"
    />
  </svg>
)

export default SVG
