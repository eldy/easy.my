import React from 'react'

const SVG = ({
  fill = '#000',
  width = '100%',
  height = '100%',
  viewBox = '0 0 25.054 24',
  classes
}) => (
  <svg
    width={width}
    height={height}
    className={classes}
    viewBox={viewBox}
    xmlns="http://www.w3.org/2000/svg"
    xmlnsXlink="http://www.w3.org/1999/xlink"
  >
    <path
      fill={fill}
      d="M20.79,4H4.088A2.042,2.042,0,0,0,2.01,6L2,18a2.051,2.051,0,0,0,2.088,2h16.7a2.051,2.051,0,0,0,2.088-2V6A2.051,2.051,0,0,0,20.79,4Zm0,4-8.351,5L4.088,8V6l8.351,5L20.79,6Z"
    />
  </svg>
)

export default SVG
