import React from 'react'
import styles from './ContactChannel.scss'

import Button from '../Button'
import IconCall from '../Icons/Call'
import IconEmail from '../Icons/Email'
import IconChat from '../Icons/Chat'

const ContactChannel = ({ phone }) => (
  <div className={styles.wrap}>
    <div className={styles.caption}>
      Interested with the ad? Contact the seller
    </div>
    <div className={styles.innerWrap}>
      <Button Icon={IconCall} iconWidth={18} iconHeight={18} value={phone} />
      <Button Icon={IconEmail} iconWidth={18} iconHeight={18} value="Email" />
      <Button Icon={IconChat} iconWidth={18} iconHeight={18} value="Chat" />
    </div>
  </div>
)

export default ContactChannel
