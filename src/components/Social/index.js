import React from 'react'
import styles from './Social.scss'
import WishlistIcon from '../../components/Icons/Wishlist'
import ShareIcon from '../../components/Icons/Share'

const social = () => (
  <div className={styles.wrap}>
    <div className={styles.innerWrap}>
      <WishlistIcon classes={styles.icon} />
      <span>Wishlist</span>
    </div>
    <div className={styles.innerWrap}>
      <ShareIcon classes={styles.icon} />
      <span>Share</span>
    </div>
  </div>
)
export default social
