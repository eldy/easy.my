import React from 'react'
import style from './Button.scss'

/* a better option would be using svg loader because adding attribute values to create 
our own icon components manually is time consuming */

const Button = ({ Icon, iconWidth, iconHeight, value }) => (
  <button className={style.btn}>
    <Icon width={iconWidth} height={iconHeight} classes={style.icon} />
    <div>{value}</div>
  </button>
)
export default Button
